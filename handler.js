'use strict';
const axios = require('axios')

module.exports.hello = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Good Hello!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
module.exports.ciao = (event, context, callback) => {
    const request = event.Records[0].cf.request;
    console.log("event", event, "request", request)

    if (request.method === 'POST') {
      console.log("req body", request.body)
      console.log("req url", request.body.uri)
        let token = request.body.uri.split('/')[2]
        /* HTTP body is always passed as base64-encoded string. Decode it. */
        let body = Buffer.from(request.body.data, 'base64').toString();
        axios.post("https://wd2bn8ei6i.execute-api.us-east-1.amazonaws.com/dev/target/"+token, body)
        .then(function (response) {
            
         console.log('success onRequest', response)
        })
        .catch(function (error) {
          
          console.log("error onRequest", error)
        })

 
       
    }
    return callback(null, request);
}



module.exports.bye = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Good Bye!',
        input: event,
      },
      null,
      2
    ),
  };
  

};
module.exports.target = async event => {
console.log(event)
console.log('target reached')
console.log('body', event.body)
console.log('token',event.pathParameters.token)
return {
  statusCode: 200,
  body: JSON.stringify(
    {
      message: 'Done !',
    },
    null,
    2
  ),
}
}